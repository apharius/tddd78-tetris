package se.liu.ida.alere980.tddd78.tetris;

public class TetrominoMaker
{
    public int getNumberofTypes(){
        return SquareType.values().length - 1;
    }

    public Poly getPoly(int n){
        if(n > getNumberofTypes()){
            throw new IllegalArgumentException("Invalid index: " + n);
	}



	switch(SquareType.values()[n+1]){
            //I, O, T, S, Z, J, L
	    case I:
	        return new Poly(new SquareType[][]
					{{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY},
					{SquareType.I,SquareType.I,SquareType.I,SquareType.I},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    case O:
	        return new Poly(new SquareType[][]
					{{SquareType.O,SquareType.O},
					 {SquareType.O,SquareType.O}});
	    case T:
	        return new Poly(new SquareType[][]
					{{SquareType.EMPTY,SquareType.T,SquareType.EMPTY},
					{SquareType.T,SquareType.T,SquareType.T},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    case S:
		return new Poly(new SquareType[][]
					{{SquareType.EMPTY,SquareType.S,SquareType.S},
					{SquareType.S,SquareType.S,SquareType.EMPTY},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    case Z:
	        return new Poly(new SquareType[][]
					{{SquareType.Z,SquareType.Z,SquareType.EMPTY},
					{SquareType.EMPTY,SquareType.Z,SquareType.Z},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    case J:
		return new Poly(new SquareType[][]
					{{SquareType.J,SquareType.EMPTY,SquareType.EMPTY},
					{SquareType.J,SquareType.J,SquareType.J},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    case L:
		return new Poly(new SquareType[][]
					{{SquareType.EMPTY,SquareType.EMPTY,SquareType.L},
					{SquareType.L,SquareType.L,SquareType.L},
					{SquareType.EMPTY,SquareType.EMPTY,SquareType.EMPTY}});
	    default:
	        return new Poly(new SquareType[][]{});
	}
    }
}
