package se.liu.ida.alere980.tddd78.tetris;
import java.util.Random;

import static java.lang.Math.min;

public class Board
{
    private SquareType[][] squares;
    private int width, height, fallingX, fallingY;
    private Poly falling;

    public Board(final int width, final int height) {
	this.width = width;
	this.height = height;

	squares = new SquareType[height][width];

	for (int y = 0; y < height ; y++) {
	    for (int x = 0; x < width; x++) {
		squares[y][x] = SquareType.EMPTY;
	    }
	}
    }

    public static void main(String[] args) {
        // Test coordinates, please ignore
	//noinspection ResultOfObjectAllocationIgnored
	new Board(57,63);
    }

    public void randomizeBoard(){
        Random rand = new Random();
        SquareType[] types = SquareType.values();
        int len = types.length;

	for (int y = 0; y < height ; y++) {
	    for (int x = 0; x < width; x++) {
		squares[y][x] = types[rand.nextInt(len)];
	    }
	}
    }

    public SquareType getCellData(int inX, int inY){
	SquareType[][] squaresWithFalling = squares.clone();

	if(falling != null){
	    SquareType[][] fallingBlocks = falling.getBlocks();

	    for (int y = fallingY; y < min(fallingY + fallingBlocks.length, height); y++) {
		for (int x = fallingX; x < min(fallingX + fallingBlocks[0].length, width); x++) {
		    squaresWithFalling[y][x] = fallingBlocks[y - fallingY][x - fallingX];
		}
	    }
	}

        return squaresWithFalling[inY][inX];
    }

    public int getWidth() {
	return width;
    }

    public int getHeight() {
	return height;
    }

    public Poly getFalling() {
	return falling;
    }

    public int getFallingX() {
	return fallingX;
    }

    public int getFallingY() {
	return fallingY;
    }

    public void setFallingX(final int fallingX) {
	this.fallingX = fallingX;
    }

    public void setFallingY(final int fallingY) {
	this.fallingY = fallingY;
    }

    public void setFalling(final Poly falling) {
	this.falling = falling;
    }
}
