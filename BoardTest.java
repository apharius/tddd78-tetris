package se.liu.ida.alere980.tddd78.tetris;

import java.awt.event.ActionEvent;

import javax.swing.*;

public final class BoardTest
{
    private BoardTest() {}

    public static void main(String[] args) {
        // test data, please ignore
        Board board = new Board(10,20);

        /*
	for (int i = 0; i < 10; i++) {
	    board.randomizeBoard();
	    System.out.println(BoardToTextConverter.convertToText(board));
	}
	*/

	TetrisFrame frame = new TetrisFrame(board);

        final Action doOneStep = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                board.randomizeBoard();
                frame.updateBoardDraw();
            }
        };

        final Timer clockTimer = new Timer(500, doOneStep);
        clockTimer.setCoalesce(true);
        clockTimer.start();
	// This isn't used since the closing of the frame currently ends the application
        //clockTimer.stop();

    }
}
