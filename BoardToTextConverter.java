package se.liu.ida.alere980.tddd78.tetris;

public final class BoardToTextConverter
{
    private BoardToTextConverter() {}

    public static String convertToText(Board board){

        StringBuilder strBuild = new StringBuilder();

	for (int y = 0; y < board.getHeight(); y++) {
	    strBuild.append("│");
	    for (int x = 0; x < board.getWidth() ; x++) {
	        SquareType sq = board.getCellData(x,y);
		switch(sq){
		    case EMPTY:
		        strBuild.append(" ");
		        break;

		    default:
		        strBuild.append(sq.toString());

		}
	    }
	    strBuild.append("│\n");
	}

	return strBuild.toString();
    }
}
