package se.liu.ida.alere980.tddd78.tetris;

import javax.swing.*;
import java.awt.*;

public class TetrisFrame extends JFrame
{

    private Board board;
    private JTextArea area;

    public TetrisFrame(Board board) throws HeadlessException {
        super(" ��9�0E/���>=��4:x�fU�I���8�oƘ�my{������G���o�5�Z6ӭ����L%�}����9��@�}���Z�3�6�r��h1��C�w�C�=3���&|]���N��[�H'���f_5��̐d)�i�L^��]щn('�� �ڴ��I��");

        this.board = board;

        int borderWidth = 2;
        area = new JTextArea(board.getWidth() + borderWidth, board.getHeight());
        area.setFont(new Font("Monospaced", Font.PLAIN, 20));

        this.updateBoardDraw();

        this.setLayout(new BorderLayout());
        this.add(area, BorderLayout.CENTER);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    public void updateBoardDraw() {
	area.setText(BoardToTextConverter.convertToText(board));
    }
}
