package se.liu.ida.alere980.tddd78.tetris;

public class Poly
{
    private SquareType[][] blocks;

    public Poly(final SquareType[][] blocks) {
	this.blocks = blocks;
    }

    public SquareType[][] getBlocks() {
	return blocks;
    }
}
