package se.liu.ida.alere980.tddd78.tetris;

public enum SquareType
{
    EMPTY, I, O, T, S, Z, J, L
}
